const { useEffect, useState } = React;


function ArtistListItem({ artist, onClick, onAddToFavorites, onRemoveFromFavorites, isFavorite }) {
    return (
      <li key={artist.id} onClick={() => onClick(artist)}>
        {artist.name}
        <button className="favoriteButton" onClick={() => (isFavorite ? onRemoveFromFavorites(artist) : onAddToFavorites(artist))}>
          {isFavorite ? "-" : "+"}
        </button>
      </li>
    );
  }

  function ArtistDetails({ selectedArtist, topTracks, albums, relatedArtists }) {
    if (!selectedArtist) {
      return null;
    }
  
    const formattedFollowers = selectedArtist.followers.total.toLocaleString();
  
    return (
      <div>
        <h1>{selectedArtist.name}</h1>
        <h2>{formattedFollowers} Monthly Listeners</h2>
        {selectedArtist.images && selectedArtist.images.length > 0 && selectedArtist.images[0].url && (
          <a href={selectedArtist.external_urls.spotify}>
            <img src={selectedArtist.images[0].url} alt="" />
          </a>
        )}
        <h3 id="genres">Top Genres: </h3>
        <ol>{selectedArtist.genres.map((genre, index) => <li key={index}>{genre}</li>)}</ol>
  
        {topTracks && (
          <div>
            <h2>Top Tracks</h2>
            <div id="topTracks">
              {topTracks.slice(0, 5).map((track) => (
                <div key={track.id}>
                  <a href={track.external_urls.spotify}>
                    {track.album.images && track.album.images.length > 0 && track.album.images[0].url && (
                      <img src={track.album.images[0].url} alt="" />
                    )}
                    {track.name}
                  </a>
                </div>
              ))}
            </div>
          </div>
        )}
  
        {albums && albums.length > 0 && (
          <div id="albums">
            <h2>Albums</h2>
            {albums.map((album) => (
              <div key={album.id}>
                <h3>{album.name}</h3>
                <p>Released: {album.release_date}</p>
                <p>Total tracks: {album.total_tracks}</p>
                {album.images && album.images.length > 0 && album.images[0].url && (
                  <a className="album_link" href={album.external_urls.spotify}>
                    <img src={album.images[0].url} alt="" className="album" />
                  </a>
                )}
              </div>
            ))}
          </div>
        )}
  
        {relatedArtists && (
          <div>
            <h2>Related Artists</h2>
            <div id="relatedArtists">
              {relatedArtists.slice(0, 5).map((artist) => (
                <div key={artist.id}>
                  <a href={artist.external_urls.spotify}>
                    {artist.images && artist.images.length > 0 && artist.images[0].url && (
                      <img src={artist.images[0].url} alt={`${artist.name} thumbnail`} />
                    )}
                    {artist.name}
                  </a>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }

function App() {
  const CLIENT_ID = "de3853d12d6b4d7aaae169e8b56bed39";
  const REDIRECT_URI = "https://sonic.dawsoncollege.qc.ca/~2237902/webassignment06/";
  const AUTH_ENDPOINT = "https://accounts.spotify.com/authorize";
  const RESPONSE_TYPE = "token";

  const [token, setToken] = useState("");
  const [searchKey, setSearchKey] = useState("");
  const [artists, setArtists] = useState([]);
  const [selectedArtist, setSelectedArtist] = useState("");
  const [albums, setAlbums] = useState("");
  const [topTracks, setTopTracks] = useState("");
  const [myRelatedArtists, setRelatedArtists] = useState("");
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem("favorites")) || [])

  useEffect(() => {
    const hash = window.location.hash;
    let token = window.localStorage.getItem("token")

    if (!token && hash) {
      token = hash
        .substring(1)
        .split("&")
        .find((elem) => elem.startsWith("access_token"))
        .split("=")[1];

      window.location.hash = ""
      window.localStorage.setItem("token", token)
    }

    setToken(token);
  }, [])

  useEffect(() => {
    if (selectedArtist) {
      artistAlbums(selectedArtist)
    }
  }, [selectedArtist])

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites))
  }, [favorites])

  const logout = () => {
    setToken("");
    setArtists([]);
    setSelectedArtist("");
    setAlbums("");
    setTopTracks("");
    setRelatedArtists([]);
    setFavorites([]);
    window.localStorage.removeItem("token");
  };

  const searchArtists = async (e) => {
    e.preventDefault();
    const { data } = await axios.get("https://api.spotify.com/v1/search", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      params: {
        q: searchKey,
        type: "artist",
      },
    })
    setArtists(data.artists.items);
  }

  const searchTopTracks = async (artist) => {
      const { data } = await axios.get(`https://api.spotify.com/v1/artists/${artist.id}/top-tracks`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          market: "ES",
        },
      });

      setTopTracks(data.tracks);
    }

  const artistAlbums = async (artist) => {
    const { data } = await axios.get(`https://api.spotify.com/v1/artists/${artist.id}/albums`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      params: {
        include_groups: "album",
        market: "ES",
      },
    })

    const correctAlbums = data.items.filter((album) =>
      album.artists.some((albumArtist) => albumArtist.name === selectedArtist.name)
    )
    setAlbums(correctAlbums)
  }

  const relatedArtists = async (artist) => {
    const { data } = await axios.get(`https://api.spotify.com/v1/artists/${artist.id}/related-artists`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    setRelatedArtists(data.artists);
  }

  const handleArtistClick = (artist) => {
    setSelectedArtist(artist)
    searchTopTracks(artist)
    relatedArtists(artist)
  }


  const handleAddToFavorites = (artist) => {
    if (!favorites.find((fav) => fav.id === artist.id)) {
      setFavorites([...favorites, artist])
    }
  }

  const handleRemoveFromFavorites = (artist) => {
    const updatedFavorites = favorites.filter((fav) => fav.id !== artist.id)
    setFavorites(updatedFavorites)
  }

  const isFavorite = (artist) => favorites.some((fav) => fav.id === artist.id)


  const renderArtists = () => {
    const displayedArtists = artists.slice(0, 10);

    return displayedArtists.map((artist) => (
        <ArtistListItem 
            key={artist.id} 
            artist={artist} 
            onClick={handleArtistClick} 
            onAddToFavorites={handleAddToFavorites}
            onRemoveFromFavorites={handleRemoveFromFavorites}
            isFavorite={isFavorite(artist)}
        />
    ))
  }

  const renderFavorites = () => {
    if (!token) {
      return null
    }
    return (
      <div>
        <h2>Favorites</h2>
        {favorites.map((artist) => (
          <ArtistListItem
            key={artist.id}
            artist={artist}
            onClick={handleArtistClick}
            onAddToFavorites={handleAddToFavorites}
            onRemoveFromFavorites={handleRemoveFromFavorites}
            isFavorite={isFavorite(artist)}
          />
        ))}
      </div>
    )
  }

  return (
    <div className="wrapper">
      <header className="App-header">
        <h1>Spotify API</h1>
        {!token ? (
          <a href={`${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}`}>
            Login to Spotify
          </a>
        ) : (
          <button id="logout" onClick={logout}>
            Logout
          </button>
        )}

        {token ? (
          <form onSubmit={searchArtists}>
            <input type="text" onChange={(e) => setSearchKey(e.target.value)} />
            <button type={"submit"}>Search</button>
          </form>
        ) : (
          <h2>Please login</h2>
        )}

        <ul>
          {renderArtists()}
        </ul>
        <ul id="favourites">
          {renderFavorites()}
        </ul>
      </header>
      
      <main>
        <ArtistDetails
          selectedArtist={selectedArtist}
          topTracks={topTracks}
          albums={albums}
          relatedArtists={myRelatedArtists}
        />
      </main>
    </div>
  );
}

ReactDOM.render(
    <App />, 
    document.body
);
